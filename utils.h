#ifndef UTILS_H
#define UTILS_H

#include <thread>
#include <mutex>
#include <queue>
#include <condition_variable>

class scoped_thread
{
    std::thread thd_;
public:
    scoped_thread(std::thread thd) : thd_(std::move(thd))  {  }

    scoped_thread(scoped_thread&& thd) = default;
    scoped_thread& operator=(scoped_thread&& thd) = default;

    template <typename... T>
    scoped_thread(T&&... args) : thd_( std::forward<T>(args)...)
    {
    }

    void join()
    {
        if (thd_.joinable())
            thd_.join();
    }

    ~scoped_thread()
    {
        if (thd_.joinable())
            thd_.join();
    }
};

template<typename T>
class thread_safe_queue
{
    std::mutex mtx;
    std::condition_variable cond;
    std::queue<T> q;
public:
    void push(T item)
    {
        std::lock_guard<std::mutex> l(mtx);
        q.push(std::move(item));
        cond.notify_one();
    }

    void pop(T& item)
    {
        std::unique_lock<std::mutex> ul(mtx);
        cond.wait(ul, [this] () { return !q.empty();});
        item = q.front();
        q.pop();
    }

    bool try_pop(T& item)
    {
        std::unique_lock<std::mutex> ul(mtx);
        if (q.empty()) return false;
        item = q.front();
        q.pop();
        return true;
    }

};

#endif // UTILS_H

