#include <iostream>
#include <vector>
#include <algorithm>
#include <future>

using namespace std;

template< class It, class T >
T parallel_accumulate_helper(It first, It last, T init )
{
//    auto res = async(launch::async, &accumulate<InputIt, T>, first, last, init);
//    return res.get();
    int N = max(thread::hardware_concurrency(), 1u);
    vector<future<T>> results;

    size_t block_size = distance(first, last)/N;
    It block_start = first;
    for (int i = 0 ; i < N ; ++i)
    {
        It block_end = block_start;
        advance(block_end, block_size);
        if( i == N-1) block_end = last;
        //results.push_back(async(launch::async, accumulate<It, T>,
        //                        block_start, block_end, T()));
        results.push_back(async(launch::async,
                               [block_start, block_end] () {
            return accumulate(block_start, block_end, T());
        }));
        block_start = block_end;
    }

    return accumulate(results.begin(), results.end(), init,
                      [] (T a, future<T>& b) { return a + b.get();});
}

template< class It, class T >
future<T> parallel_accumulate(It first, It last, T init )
{
    return async(launch::async, parallel_accumulate_helper<It, T>,
                 first, last, init);
}

int main()
{
    vector<long> v;
    for(long i = 0 ; i < 100001 ; ++i)
        v.push_back(i);

    auto start = chrono::high_resolution_clock::now();
    cout << "sum = " << accumulate(v.begin(), v.end(), 0L) << endl;
    auto end = chrono::high_resolution_clock::now();
    cout << "Elapsed acc = ";
    cout << chrono::duration_cast<chrono::microseconds>(end-start).count();
    cout << " us" << endl;

    start = chrono::high_resolution_clock::now();
    auto res = parallel_accumulate(v.begin(), v.end(), 0L);
    cout << "parallel sum = " << res.get() << endl;
    end = chrono::high_resolution_clock::now();
    cout << "Elapsed parallel acc = ";
    cout << chrono::duration_cast<chrono::microseconds>(end-start).count();
    cout << " us" << endl;
    return 0;
}

