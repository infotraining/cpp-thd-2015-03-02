#include <iostream>
#include <future>
#include <thread>
#include "../../utils.h"

using namespace std;

int may_throw(int id)
{
    if(id == 13)
        throw runtime_error("bad luck");
    return id;
}

int question()
{
    this_thread::sleep_for(chrono::seconds(1));
    return 42;
}

class my_package_task
{
    std::function<int()> f;
    std::promise<int> p;
public:
    my_package_task(std::function<int()> f)  : f(f)
    {

    }

    void operator()()
    {
        p.set_value(f());
    }

    future<int> get_future()
    {
        return p.get_future();
    }
};

int main()
{
    future<int> res = async(launch::async, question);
    cout << "waitng for result" << endl;
    res.wait();
    cout << "Res = " << res.get() << endl;

    future<int> throwable_result = async(launch::async, may_throw, 13);
    throwable_result.wait();
    try {
        cout << "Throwable result = " << throwable_result.get() << endl;
    }
    catch(...)
    {
        cerr << "an exception has occured" << endl;
    }

    //packaged_task<int()> pt(question);
    my_package_task pt(question);
    future<int> res2 = pt.get_future();
    scoped_thread th1(move(pt)); // async
    //pt(); //defer

    cout << "waitng for result" << endl;
    res2.wait();
    cout << "Res = " << res2.get() << endl;


    return 0;
}

