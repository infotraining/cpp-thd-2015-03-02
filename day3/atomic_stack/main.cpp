#include <iostream>
#include "../../utils.h"
#include <atomic>

using namespace std;

struct Node
{
    Node* prev;
    int data;
};

class Stack
{
    atomic<Node*> head{nullptr};
public:
    void push(int item)
    {
        Node* new_node = new Node;
        new_node->prev = head.load();
        new_node->data = item;
        //head = new_node; <- still race
        while(!head.compare_exchange_weak(new_node->prev, new_node));
    }

    int pop()
    {
        Node* old_node = head.load();
        while(!head.compare_exchange_weak(old_node, old_node->prev));
        int res = old_node->data;
        delete old_node;
        return res;
    }
};

void test_stack(Stack& s)
{
    for (int i = 0 ; i < 100 ; ++i)
    {
        s.push(i);
    }
    for (int i = 0 ; i < 100 ; ++i)
    {
        cout << s.pop() << ", ";
    }
    cout << endl;
}

int main()
{
    Stack s;
    scoped_thread t1(test_stack, ref(s));
    scoped_thread t2(test_stack, ref(s));
    return 0;
}

