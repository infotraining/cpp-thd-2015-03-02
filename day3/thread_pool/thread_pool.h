#ifndef THREAD_POOL_H
#define THREAD_POOL_H
#include <functional>
#include <future>
#include "../../utils.h"

using task_t = std::function<void()>;

class thread_pool
{
    thread_safe_queue<task_t> q;
    std::vector<scoped_thread> workers;

public:
    thread_pool(int n_of_workers)
    {
        for (int i = 0 ; i < n_of_workers ; ++i)
            workers.emplace_back([this]()
            {
               for(;;)
               {
                   task_t task;
                   q.pop(task);
                   if(!task)
                        return;
                   task();
               }
            });
    }

    ~thread_pool()
    {
        for (int i = 0 ; i < workers.size() ; ++i)
            q.push(nullptr);
    }

    void submit(task_t task)
    {
        q.push(task);
    }

//    template<typename F, typename... Arg>
//    auto async(F f, Arg&&... args) -> std::future<decltype(f(args...))>
//    {
//        using res_t = decltype(f(args...));
//    }
    template<typename F>
    auto async(F f) -> std::future<decltype(f())>
    {
        using res_t = decltype(f());
        auto task = std::make_shared<std::packaged_task<res_t()>>(f);
        auto res = task->get_future();
        q.push([task] {(*task)();});
        return res;
    }
};

#endif // THREAD_POOL_H

