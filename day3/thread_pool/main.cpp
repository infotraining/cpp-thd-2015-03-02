#include <iostream>
#include <functional>
#include <thread>
#include "thread_pool.h"

using namespace std;

int answer(double question)
{
    return 42;
}

void process(std::function<int(double)> fun)
{
    fun(3.14);
}

struct funky
{
    int operator()(double a)
    {
        return 47;
    }
};

int question()
{
    this_thread::sleep_for(chrono::seconds(1));
    return 42;
}

void hello_from_fun()
{
    cout << "hello from fun" << endl;
}

int main()
{
//    cout << "Hello World!" << endl;
//    process(answer);
//    process([](double a) { return static_cast<int>(a);});
//    process(funky());
    thread_pool tp(4);
    tp.submit(hello_from_fun);
    tp.submit([]() { cout << "this is lambda speaking" << endl;});
    for (int i = 0 ; i < 50 ; ++i)
        tp.submit([i] () {
            this_thread::sleep_for(chrono::milliseconds(500));
            cout << "this is " << i << " lambda" << endl;
        });
    future<int> res = tp.async(question);
    cout << "Answer is " << res.get() << endl;
    return 0;
}

