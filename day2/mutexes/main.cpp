#include <iostream>
#include <vector>
#include <thread>
#include <mutex>
#include <chrono>
#include <atomic>

using namespace std;

class LockGuard
{
    mutex& mtx;
public:
    LockGuard(const LockGuard&) = delete;
    LockGuard& operator=(const LockGuard&) = delete;
    LockGuard(mutex& mtx) : mtx(mtx)
    {
        mtx.lock();
    }
    ~LockGuard()
    {
        mtx.unlock();
    }
};

long counter{};
mutex counter_mtx;

void inc()
{
    counter++;
    if (counter == 1000)
        throw std::logic_error("my fault");
}

void inc_counter()
{
    try {
        for (int i = 0 ; i < 100000 ; ++i)
        {
            lock_guard<mutex> lg(counter_mtx);
            inc();
        }
    }
    catch (...)
    {
        cerr << "Error occured in : " << this_thread::get_id() << endl;
    }
}


int main()
{
    vector<thread> thds;


    auto start = chrono::high_resolution_clock::now();

    for (int i = 0 ; i < 4 ; ++i)
    {
        thds.emplace_back(inc_counter);
    }
    for(auto& th :thds) th.join();

    auto end = chrono::high_resolution_clock::now();
    cout << "Elapsed: ";
    cout << chrono::duration_cast<chrono::microseconds>(end-start).count();
    cout << " us - mutex" << endl;

    cout << "Counter with mutex = " << counter << endl;


    return 0;
}

