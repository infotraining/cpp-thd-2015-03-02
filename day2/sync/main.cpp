#include <iostream>
#include <vector>
#include <thread>
#include "../../utils.h"
#include <algorithm>
#include <atomic>
#include <condition_variable>

using namespace std;

namespace atom
{
class Data
{
    vector<int> data_;
    atomic<bool> is_ready {false};
    //bool is_ready{false}; <- wrong, optimized out of for loop
    //volatile bool is_ready{false}; <- not so good

public:
    void read()
    {
        cout << "Reading data" << endl;
        this_thread::sleep_for(chrono::seconds(2));
        data_.resize(10);
        generate(data_.begin(), data_.end(), [] { return rand() % 100;});
        is_ready = true;

        cout << "data is ready" << endl;
    }

    void process()
    {
        for(;;)
        {
            //std::atomic_thread_fence(std::memory_order_seq_cst);
            if (is_ready)
            {
                cout << "processing, sum = ";
                cout << accumulate(data_.begin(), data_.end(), 0) << endl;
                return;
            }
            //this_thread::yield();
            this_thread::sleep_for(chrono::milliseconds(20));
        }
    }
};
}

namespace cond
{
class Data
{
    vector<int> data_;
    condition_variable cond;
    bool is_ready{false};
    mutex mtx;

public:
    void read()
    {
        cout << "Reading data cond" << endl;
        this_thread::sleep_for(chrono::seconds(10));
        data_.resize(10);
        generate(data_.begin(), data_.end(), [] { return rand() % 100;});
        {
            lock_guard<mutex> lg(mtx);
            is_ready = true;
        }
        cond.notify_one();
        cout << "data is ready" << endl;
    }

    void process()
    {
        unique_lock<mutex> lk(mtx);
//        while (!is_ready)
//            cond.wait(lk);
        cond.wait(lk, [this] { return is_ready;});

        cout << "processing, sum = ";
        cout << accumulate(data_.begin(), data_.end(), 0) << endl;
    }
};
}

using namespace cond;

int main()
{
    Data data;
    scoped_thread th1(&Data::read, &data);
    scoped_thread th2(&Data::process, &data);
    return 0;
}

