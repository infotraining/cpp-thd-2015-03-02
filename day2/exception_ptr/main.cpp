#include <iostream>
#include <exception>
#include <thread>
#include <vector>
#include "../../utils.h"

using namespace std;

void may_throw(int v)
{
    if (v == 13)
        throw logic_error("bad luck!");
}

void worker(int v, exception_ptr& ptr)
{
    try {
        may_throw(v);
    }
    catch(...)
    {
        ptr = std::current_exception();
    }
}

int main()
{
    cout << "exceptions test" << endl;
    exception_ptr eptr;
    {
        scoped_thread th1(worker, 13, ref(eptr));
    }
    if (eptr)
    {
        // we got exception in eptr
        try
        {
            std::rethrow_exception(eptr);
        }
        catch(const logic_error& err)
        {
            cerr << "got error in thread " << err.what() << endl;
        }
    }
    return 0;
}

