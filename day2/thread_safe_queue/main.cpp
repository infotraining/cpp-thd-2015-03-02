#include <iostream>
#include <vector>
#include <thread>
#include "../../utils.h"
#include <algorithm>
#include <atomic>
#include <condition_variable>
#include <queue>

using namespace std;

thread_safe_queue<int > q;

void producer()
{
    for (int i = 0 ; i < 10 ; ++i)
    {
        this_thread::sleep_for(chrono::milliseconds(100));
        q.push(i);
    }
    q.push(-1);
}

void consumer(int id)
{
    for (;;)
    {
        int msg;
        q.pop(msg);
        if (msg == -1)
        {
            q.push(-1);
            return;
        }
        cout << "Consumer " << id << " got " << msg << endl;
    }
}

int main()
{
    scoped_thread p(producer);
    scoped_thread c1(consumer, 1);
    scoped_thread c2(consumer, 2);
    return 0;
}

