#include <iostream>
#include <vector>
#include <thread>
#include <mutex>
#include <chrono>
#include <atomic>
#include "../../utils.h"

using namespace std;



long counter{};
mutex counter_mtx;

void inc()
{
    counter++;
    if (counter == 1000)
        throw std::logic_error("my fault");
}

void inc_counter()
{
    try {
        for (int i = 0 ; i < 100000 ; ++i)
        {
            lock_guard<mutex> lg(counter_mtx);
            inc();
        }
    }
    catch (...)
    {
        cerr << "Error occured in : " << this_thread::get_id() << endl;
    }
}


int main()
{
    vector<scoped_thread> thds;


    auto start = chrono::high_resolution_clock::now();

    {
        for (int i = 0 ; i < 4 ; ++i)
        {
            thds.emplace_back(inc_counter);
        }
    }
    {
        //thread th1(inc_counter);
        scoped_thread th(thread(inc_counter));
        scoped_thread t2(inc_counter);
    }

    auto end = chrono::high_resolution_clock::now();
    cout << "Elapsed: ";
    cout << chrono::duration_cast<chrono::microseconds>(end-start).count();
    cout << " us - mutex" << endl;

    cout << "Counter with mutex = " << counter << endl;


    return 0;
}

