#include <iostream>
#include <thread>
#include <mutex>
#include "../../utils.h"

using namespace std;

timed_mutex mtx;

void worker()
{
    cout << "Worker started" << endl;
    unique_lock<timed_mutex> lg(mtx, try_to_lock);
    if (!lg.owns_lock())
    {
        do
        {
            cout << "Waiting for mutex " << this_thread::get_id() << endl;
            //this_thread::sleep_for(chrono::milliseconds(500));
        }
        while(!lg.try_lock_for(chrono::milliseconds(500)));
    }
    cout << "i have mutex " << this_thread::get_id() << endl;
    this_thread::sleep_for(chrono::seconds(2));
}

int main()
{
    cout << "Hello World!" << endl;
    scoped_thread th1(worker);
    scoped_thread th2(worker);
    return 0;
}

