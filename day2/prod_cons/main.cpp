#include <iostream>
#include <vector>
#include <thread>
#include "../../utils.h"
#include <algorithm>
#include <atomic>
#include <condition_variable>
#include <queue>

using namespace std;

queue<int > q;
mutex mtx;
condition_variable cond;

void producer()
{
    for (int i = 0 ; i < 100 ; ++i)
    {
        this_thread::sleep_for(chrono::milliseconds(100));
        {
            lock_guard<mutex> l(mtx);
            q.push(i);
            cond.notify_one();
        }
    }
    q.push(-1);
    cond.notify_all();
}

void consumer(int id)
{
    for (;;)
    {
        unique_lock<mutex> ul(mtx);
        while (q.empty())
            cond.wait(ul);
        int msg = q.front();
        if (msg == -1) return;
        q.pop();
        cout << "Consumer " << id << " got " << msg << endl;
    }
}

int main()
{
    scoped_thread p(producer);
    scoped_thread c1(consumer, 1);
    scoped_thread c2(consumer, 2);
    return 0;
}

