#include <iostream>
#include <thread>
#include <vector>

using namespace std;

void fun()
{
    cout << "id: " << this_thread::get_id()  << endl;
    this_thread::sleep_for(chrono::milliseconds(500)); // C++11
    this_thread::sleep_for(500ms);
    this_thread::yield();
    cout << "end of thread" << endl;
}

int main()
{
    cout << "Hello World!" << endl;
    cout << "Physical thread count " << thread::hardware_concurrency() << endl;
    thread th1(fun);
    cout << "is joinable " <<  th1.joinable() << endl;
    th1.detach();
    cout << "is joinable " <<  th1.joinable() << endl;
    //th1.detach();
    return 0;
}

