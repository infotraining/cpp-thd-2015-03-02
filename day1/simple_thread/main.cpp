#include <iostream>
#include <thread>

using namespace std;

void fun()
{
    cout << "hello from thread" << endl;
}

void hello(int id)
{
    cout << "Hello from " << id << endl;
}

void answer(int& res)
{
    res = 42;
}

struct Callable
{
    void operator()(int id)
    {
        cout << "Callable " << id << endl;
    }
};



int main()
{
    cout << "Hello World!" << endl;
    thread th1(&fun);
    thread th2(&hello, 10);
    int res{};
    thread th3(answer, ref(res));
    Callable cal;
    thread th4(cal, 10);
    cout << "My main thread" << endl;
    th1.join();
    th2.join();
    th3.join();
    th4.join();
    cout << "Result = " << res << endl;
    return 0;
}

