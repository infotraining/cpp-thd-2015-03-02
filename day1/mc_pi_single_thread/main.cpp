#include <iostream>
#include <random>
#include <thread>

using namespace std;

void pi_calc(long N, long& cnt)
{
    random_device rd;
    mt19937_64 gen(rd());
    uniform_real_distribution<> dis(0,1);

    for (int i = 0 ; i < N ; ++i)
    {
        double x = dis(gen);
        double y = dis(gen);
        if( x*x + y*y < 1)
            ++cnt;
    }
}

int main()
{
    long N = 1000000000;
    long cnt {};
    pi_calc(N, ref(cnt));

    cout << "Pi = " << double(cnt)/double(N) * 4 << endl;
    return 0;
}

