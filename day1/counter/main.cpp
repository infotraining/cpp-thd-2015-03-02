#include <iostream>
#include <vector>
#include <thread>
#include <mutex>
#include <chrono>
#include <atomic>

using namespace std;

auto counter = 0L;
mutex mtx;

void inc_counter()
{
    for (int i = 0 ; i < 100000 ; ++i)
    {        
        //mtx.lock();
        ++counter;
        //mtx.unlock();
    }
}

atomic<long> cnt_atomic{0};

void atomic_counter()
{
    for (int i = 0 ; i < 100000 ; ++i)
    {
        cnt_atomic.fetch_add(1);
    }
}


int main()
{
    vector<thread> thds;
    atomic<double> dbl_at;
    cout << "is atomic double lock free " << dbl_at.is_lock_free() << endl;

    auto start = chrono::high_resolution_clock::now();

    for (int i = 0 ; i < 4 ; ++i)
    {
        thds.emplace_back(inc_counter);
    }
    for(auto& th :thds) th.join();

    auto end = chrono::high_resolution_clock::now();
    cout << "Elapsed: ";
    cout << chrono::duration_cast<chrono::microseconds>(end-start).count();
    cout << " us - mutex" << endl;

    cout << "Counter with mutex = " << counter << endl;

    start = chrono::high_resolution_clock::now();

    thds.clear();
    for (int i = 0 ; i < 4 ; ++i)
    {
        thds.emplace_back(atomic_counter);
    }
    for(auto& th :thds) th.join();

    end = chrono::high_resolution_clock::now();
    cout << "Elapsed: ";
    cout << chrono::duration_cast<chrono::microseconds>(end-start).count();
    cout << " us - atomic" << endl;

    cout << "Counter atomic = " << cnt_atomic.load() << endl;
    cout << "Was it lock free? " << cnt_atomic.is_lock_free() << endl;

    return 0;
}

