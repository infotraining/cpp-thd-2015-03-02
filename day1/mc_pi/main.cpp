#include <iostream>
#include <random>
#include <thread>

using namespace std;

void pi_calc(long N, long& cnt)
{
    random_device rd;
    mt19937_64 gen(rd());
    uniform_real_distribution<> dis(0,1);

    double x, y;
    for (int i = 0 ; i < N ; ++i)
    {
        x = dis(gen);
        y = dis(gen);
        if( x*x + y*y < 1)
            ++cnt;
    }
}

int main()
{
    //int n_of_thds = thread::hardware_concurrency();
    int n_of_thds = 64 ;
    vector<thread> thds;
    vector<long> cnt(n_of_thds);
    long N = 100000000;

    for (int i = 0 ; i < n_of_thds ; ++i)
    {
        thds.emplace_back(pi_calc, N/n_of_thds, ref(cnt[i]));
    }

    for (auto& th : thds)
        th.join();


    cout << "Pi = " << std::accumulate(cnt.begin(), cnt.end(), 0L)/double(N) * 4 << endl;
    return 0;
}

