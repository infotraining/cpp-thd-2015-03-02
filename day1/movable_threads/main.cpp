#include <iostream>
#include <vector>
#include <thread>

using namespace std;

void fun()
{
    cout << "hello from thread" << endl;
}

void hello(int id)
{
    cout << "Hello from " << id << endl;
}

auto generate_thread() -> thread
{
    int id = 100;
    return thread(hello, id);
}

class Task
{
    vector<int> buff;
public:
    Task() {}
    void assign(const vector<int>& src)
    {
        buff.assign(src.begin(), src.end());
    }
    vector<int> data() const
    {
        return buff;
    }
};

auto main() -> int
{
    cout << "Hello moveable thread!" << endl;
    vector<thread> threads;
    threads.push_back(thread(fun));
    threads.push_back(thread(hello, 10));

    int id = 42;
    threads.emplace_back([&id] () -> void { cout << "From lambda " << id << endl;} );

    threads.emplace_back(fun);
    threads.emplace_back(generate_thread());
    threads.emplace_back(hello, 100);

    vector<int> v {1,2,3,4};
    Task t;
    thread th1(&Task::assign, &t, cref(v));
    thread th2([&t, &v]() { t.assign(v);});
    th1.join();
    th2.join();
    for (auto& el : t.data())
        cout << el << ", ";

    for (auto& th : threads)
        th.join();

//    for (auto it = begin(threads) ; it != end(threads) ; ++it)
//    {
//        auto& th = *it;
//        th.join();
//    }

    return 0;
}


